<?php
/**
 * The template for displaying single posts and pages.
 * Template Name: Global Template
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage ttcustom
 * @since ttcustom 1.0
 */

get_header();
?>

<main id="site-content" role="main" style="background-color: blue;">

	<?php

	if ( have_posts() ) {

		while ( have_posts() ) {
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );
		}
	}

	?>

</main><!-- #site-content -->

<?php get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
